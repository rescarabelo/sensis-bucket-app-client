import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppService} from "./app.service";
import {Subscription} from "rxjs";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  uploading = false;
  form: FormGroup;
  firstHalf: any;
  secondHalf: any;
  showFilesSubscription?: Subscription;
  uploadFileSubscription?: Subscription;
  loading = false;
  successful = false;

  constructor(
    private service: AppService
  ) {
    this.form = new FormGroup({
      file: new FormControl('')
    })

    this.showFilesSubscription = this.service.showFiles().subscribe(
      (res) => {
        this.firstHalf = (res as []).slice(0, ((res as []).length / 2) + 1);
        this.secondHalf = (res as []).slice(((res as []).length / 2) + 1);
      }
    )
  }

  ngOnInit(): void {
  }

  onFileSubmit(): void {
    this.loading = true;

    const formData = new FormData();
    // @ts-ignore
    formData.append('file', this.form.get('file').value)


    this.uploadFileSubscription = this.service.sendFile(formData).subscribe(
      (res) => {
        alert(
          `${res.filename} foi enviado com sucesso!`
        );
        this.loading = false;
        window.location.reload();
      }
    )
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      console.log(file)
      // @ts-ignore
      this.form.get('file').setValue(file);
    }
  }

  ngOnDestroy(): void {
    this.showFilesSubscription?.unsubscribe();
    this.uploadFileSubscription?.unsubscribe();
  }
}
