import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) {
  }

  showFiles(): Observable<Object> {
    return this.http.get("http://sensisspringapp-env.eba-pqnmtabi.us-east-1.elasticbeanstalk.com");
  }

  sendFile(data: any) {
    return this.http.post<any>("http://sensisspringapp-env.eba-pqnmtabi.us-east-1.elasticbeanstalk.com", data);
  }
}
